import redis
from rq import Connection, Worker

REDIS_URL = 'redis://ex-07-redis-master:6379'

if __name__ == '__main__':
    redis_connection = redis.from_url(REDIS_URL)
    with Connection(redis_connection):
        worker = Worker('default')
        worker.work()

