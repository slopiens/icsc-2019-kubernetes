#!/bin/bash

ProgName=$(basename $0)

sub_help(){
    echo "Usage: $ProgName <subcommand> [options]\n"
    echo "Subcommands:"
    echo "    docker <tag-id>   Build and push docker image"
    echo "    dashboard         Run kubernets dashboard"
    echo ""
}

sub_docker(){
    if [ -z "$1" ]
    then
        echo "[ERROR] missing argument: tag-id of docker image"
        echo "[TIP] the tag-id is your image versioning number, increment it by 1 every time"
        exit 1
    fi


    if $(grep gitlab-registry.cern.ch ~/.docker/config.json)
    then
      echo "[INFO] Already logged in gitlab-registry.cern.ch"
    else
      echo "[WARNING] Not logged in gitlab-registry.cern.ch"
      echo "[INFO] Running docker login, insert your CERN credentials"
      echo "$ docker login gitlab-registry.cern.ch"
      docker login gitlab-registry.cern.ch
    fi


    echo "[INFO] Running docker build and docker push..."
    base="gitlab-registry.cern.ch/icsc-2019/repo-cluster-${cluster_id}"
    tag=$1
    cluster_id=$(hostname | tr -dc '0-9')
    image=$(ls ex_*_app.py | tr "_" "-")
    image="${image%.*}"
    fullPath=${base}/${image}:${tag}
    echo "$ docker build -t ${fullPath} ."
    docker build -t ${fullPath} .
    echo "$ docker push ${fullPath}"
    docker push ${fullPath}

    echo "[INFO] If you didn't get any error your image is now available at: '${fullPath}'"
}

sub_dashboard(){
    echo "[INFO] To access the dashboard visit the following URL from browser:"
    echo "localhost:8001/api/v1/namespaces/kube-system/services/https:kubernetes-dashboard:/proxy/"
    echo
    echo "[INFO] Use the following token (select 'Token' option from the dashboard login page):"
    echo
    kubectl -n kube-system describe secret $(kubectl -n kube-system get secrets  | grep admin-token | awk '{print $1}') | grep "token:" | awk '{print $2}'
    echo
    echo "[INFO] Running 'kubectl proxy'... Use CTRL-C to exit:"
    echo "$ kubectl proxy --address='0.0.0.0' --accept-hosts='.*'"
    kubectl proxy --address='0.0.0.0' --accept-hosts='.*'
}

subcommand=$1
case $subcommand in
    "" | "-h" | "--help")
        sub_help
        ;;
    *)
        shift
        sub_${subcommand} $@
        if [ $? = 127 ]; then
            echo "Error: '$subcommand' is not a known subcommand." >&2
            echo "       Run '$ProgName --help' for a list of known subcommands." >&2
            exit 1
        fi
        ;;
esac
