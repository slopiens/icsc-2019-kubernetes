# Exercise 05 - Connecting front-end and back-end

We were going to improve even more  the dynamic of the message. Let's have a back-providing the message.

## 1. Implement a back-end and front-end communication

1. Edit `ex_05_frontend.py` to accept messages from the backend service and render them.
2. Edit the `Dockerfile`, you can specify the command from the Kubernetes deplpoyment description.
