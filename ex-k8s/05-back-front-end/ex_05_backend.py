import os
import platform

from flask import Flask, render_template

app = Flask(__name__)
app.env = 'development'



@app.route('/', methods=['GET', 'POST'])
def index():
    msg = 'Hello World, here are a bunch of info!\n'
    msg += 'Hostname: {}\n'.format(platform.node())
    msg += 'Platform: {}\n'.format(platform.platform())

    print('[INFO] Message is {}'.format(msg))

    return msg

if __name__ == '__main__':
    print('[INFO] Starting flask server app {}...'.format(app))
    print('[INFO] New message: {}'.format(os.environ.get('MESSAGE', "env variable 'MESSAGE' not set")))
    port = int(os.environ.get('PORT', 8080))
    app.run(host='0.0.0.0', port=port, debug=True)
    print('[INFO] Exiting...')
