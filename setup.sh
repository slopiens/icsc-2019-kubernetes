#!/bin/bash

if [ $# -eq 0 ]
  then
    echo "[ERROR] No arguments supplied. Pass your student number (two digits)"
    return 1
fi

export ICSC_SID=${1}

if ! [[ ${#ICSC_SID} -eq 2 ]] || ! [[ ${ICSC_SID} =~ ^[0-9]+$ ]]; then
    echo "[ERROR] Pass your student number (two digits)"
    return 1
fi

echo "[INFO] You are setup as student nr: ${ICSC_SID}"
echo "[INFO] You will work with cluster nr: ${ICSC_SID}"


# https://stackoverflow.com/questions/59895/getting-the-source-directory-of-a-bash-script-from-within
SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done
export ICSC_BASE_DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null && pwd )"

echo "[INFO] Setting up your kube config file..."
CONFIG_DIR=${ICSC_BASE_DIR}/clusters/cluster-${ICSC_SID}
CONFIG_DIR_ESCAPED="$(echo "$CONFIG_DIR" | sed 's_/_\\/_g')"
#echo "[DEBUG] ${CONFIG_DIR_ESCAPED}"
sed "s/@CONFIG_DIR@/$CONFIG_DIR_ESCAPED/" ${CONFIG_DIR}/config.template > ${CONFIG_DIR}/config

export KUBECONFIG=${CONFIG_DIR}/config

# kubectl autocompletion
echo "[INFO] Setting up kubectl autocompletion..."
if [ -n "$ZSH_VERSION" ]; then
  if [ $commands[kubectl] ]; then
    source <(kubectl completion zsh)
  fi
elif [ -n "$BASH_VERSION" ]; then
   source <(kubectl completion bash)
fi

echo "[INFO] Setting 'vim' as default kube editor..."
export KUBE_EDITOR=vim


echo "[INFO] Testing 'kubectl get node' command..."
echo "$ kubectl get node"
kubectl get node
