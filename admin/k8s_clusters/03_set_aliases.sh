#!/bin/bash

# source admin/openrc 

for sid in $(seq -f "%02g" 11 20); do

  source setup.sh ${sid}

  i=1
  for minion in $(kubectl get node | grep minion | awk '{print $1}'); do

    echo "$ openstack server set --property landb-alias=icsc-cluster-${sid}--load-${i}- ${minion}"
    openstack server set --property landb-alias=icsc-cluster-${sid}--load-${i}- ${minion}

    i=$((i+1))
  done
done
