#!/bin/bash

sid=$(hostname | tr -dc '0-9')
echo "[INFO] yum install a bunch of stuff..."
yum install -y docker git
yum install -y bash-completion bash-completion-extras
cp kubernetes.repo /etc/yum.repos.d/kubernetes.repo
yum install -y kubectl

echo "[INFO] adding user student-${sid}..."
adduser student-${sid}
echo icsc2019 | passwd student-${sid} --stdin

echo "[INFO] setting up docker..."
groupadd docker
usermod -aG docker rpoggi
usermod -aG docker student-${sid}
systemctl enable docker
systemctl start docker


firewall-cmd --zone=public --add-port=8001/tcp --permanent
firewall-cmd --reload
